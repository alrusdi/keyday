# -*- coding: utf-8 -*-
from common.helpers import get_current_locale

def locale(request):
    return {'current_lang': get_current_locale(request)}
