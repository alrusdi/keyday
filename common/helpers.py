# -*- coding: utf-8 -*-
import os
import json
from django.conf import settings


def get_current_locale(request):
    loc = 'ru'
    domain = request.META.get('HTTP_HOST')
    if not domain:
        return loc
    parts = domain.split('.')

    if len(parts) < 3:
        return loc

    new_loc = parts[-3]
    path = os.path.join(settings.BASE_DIR, 'static', 'locale', '%s.json' % new_loc)
    if len(loc) != 2 or not os.path.exists(path):
        return loc
    return new_loc


def translate(inp, request=None, lang='ru'):
    if request:
        lang = get_current_locale(request)
    path = os.path.join(settings.BASE_DIR, 'static', 'locale', '%s.json' % lang)
    loc = {}
    with open(path, 'r') as f:
        loc = json.loads(f.read())
    return loc.get(inp, inp)
