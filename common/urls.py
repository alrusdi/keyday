"""keyday URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.views import LogoutView
from keys.views import CreateKeyView, CommitKeyView, ViewKeyView
from profiles.views import (
    DashboardKeysView,
    HomeView,
    DashboardProgressView,
    DashboardSettingsView,
    LoginView,
    DashboardSecurityView,
    DashboardManualView,
    DashboardInfoView
)

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('info/', DashboardInfoView.as_view(), name='info'),
    path('keys/', DashboardKeysView.as_view(), name='keys'),
    path('progress/', DashboardProgressView.as_view(), name='progress'),
    path('settings/', DashboardSettingsView.as_view(), name='settings'),
    path('security/', DashboardSecurityView.as_view(), name='security'),

    path('manual/<slug:lang>/<int:level>/', DashboardManualView.as_view(), name='manual'),

    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),


    path('create-key/<int:level>/', CreateKeyView.as_view(), name='create_key'),

    path('commit-key/<int:key>/', CommitKeyView.as_view(), name='commit_key'),
    path('view/key/<int:key>/', ViewKeyView.as_view(), name='view_key'),

    path('adminio77/', admin.site.urls),
]
