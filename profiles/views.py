# -*- coding: utf-8 -*-
import os
from common.helpers import translate, get_current_locale
from common.views import JsonView
from django.conf import settings
from django.contrib.auth import authenticate, login, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse
from django.shortcuts import redirect
from profiles.forms import ContactsForm, ChangePasswordForm
from profiles.models import User
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView, FormView
from keys.models import Key

decorators = [never_cache, login_required]


class HomeView(TemplateView):
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('/keys/')
        return super().get(request, *args, **kwargs)


@method_decorator(decorators, name='dispatch')
class DashboardKeysView(TemplateView):
    template_name = 'keys.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_page'] = 'dashboard_keys'
        qs = Key.objects.filter(seller=self.request.user)
        ss = self.request.GET.get('search_string')
        if ss:
            ss = ss.strip()[0:200]
            qs = qs.filter(value__icontains=ss)
        qs = qs.order_by('-pk')
        ctx['keys'] = qs

        usr = self.request.user
        lvl = usr.level

        key_gen_info = []
        for k in range(1, 5):
            if k > lvl:
                break
            l = settings.LEVELS_INFO[k]
            lim = l['limit']

            lvl_count = Key.objects.filter(seller=usr, initial_limit=lim).count()
            is_unlimited = lvl > l['id'] or lvl > 3
            lim_count = 'ထ' if is_unlimited else lim
            btn_caption = '%s %s (%s/%s)' % (l['id'], translate('LEVEL', self.request), lvl_count, lim_count)

            is_available = True
            if lvl < 4:
                if lvl == l['id']:
                    if lvl_count >= l['limit']:
                        is_available = False

            key_gen_info.append({
                'is_available': is_available,
                'caption': btn_caption,
                'level_id': l['id'],
                'color': l['color']
            })
        ctx['key_gen_info'] = key_gen_info

        return ctx


@method_decorator(decorators, name='dispatch')
class DashboardInfoView(TemplateView):
    template_name = 'info.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_page'] = 'dashboard_info'

        usr = self.request.user

        level_sellers = []
        if usr.level < 4:
            for lvl in range(1,5):
                if lvl <= usr.level:
                    continue
                l = settings.LEVELS_INFO[lvl]
                level_sellers.append({
                    'level_info': l,
                    'seller': usr.get_seller_for_level_up(),
                })
                break

        ctx['level_sellers'] = level_sellers
        return ctx


@method_decorator([login_required], name='dispatch')
class DashboardSettingsView(FormView):
    template_name = 'profiles/contacts.html'
    success_url = '/settings/'

    def get_form(self, form_class=None):
        r = self.request
        data = r.POST if r.method == 'POST' else None
        form = ContactsForm(data, instance=self.request.user)
        return form

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_page'] = 'dashboard_settings'
        ctx['current_subpage'] = 'dashboard_settings_contacts'
        return ctx

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


@method_decorator([login_required], name='dispatch')
class DashboardSecurityView(FormView):
    template_name = 'profiles/security.html'
    success_url = '/security/'

    def get_form(self, form_class=None):
        r = self.request
        data = r.POST if r.method == 'POST' else None
        form = ChangePasswordForm(r.user, data)
        return form

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_page'] = 'dashboard_settings'
        ctx['current_subpage'] = 'dashboard_settings_security'
        return ctx

    def form_valid(self, form):
        usr = form.user
        usr.set_password(
            form.cleaned_data['new_password']
        )
        usr.save()
        update_session_auth_hash(self.request, usr)
        return super().form_valid(form)


@method_decorator(decorators, name='dispatch')
class DashboardProgressView(TemplateView):
    template_name = 'progress.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['current_page'] = 'dashboard_progress'
        ctx['current_level'] = self.request.user.level
        ctx['next_level'] = self.request.user.level + 1
        ctx['progress'] = ctx['current_level'] * 25
        return ctx


@method_decorator(decorators, name='dispatch')
class DashboardManualView(TemplateView):
    def get(self, request, *args, **kwargs):
        lvl = self.request.user.level
        wanted_lvl = int(kwargs.get('level'))
        if lvl < wanted_lvl:
            raise Http404()
        lng = kwargs.get('lang')[0:2]
        file_name = 'KeyDay-level%s.%s.pdf' % (wanted_lvl, lng)
        f = os.path.join(settings.BASE_DIR, 'files', file_name)
        if not os.path.exists(f):
            raise Http404()
        fsock = open(f, 'rb')
        response = HttpResponse(fsock, content_type='application/pdf')
        response['Content-Disposition'] = "attachment; filename=%s" % file_name
        return response


class LoginView(JsonView):
    def _validate_settings(self):
        telegram = (self.request.POST.get('telegram') or '').strip().lower()
        email = (self.request.POST.get('email') or '').strip().lower()

        if not email and not telegram:
            return 'error', 'PASSWORD_CONFIRMATION_REQUIRED'

        if not telegram:
            return 'error', 'TELEGRAM_REQUIRED'

        if not email:
            return 'error', 'EMAIL_REQUIRED'
        else:
            if User.objects.filter(username=email).exists():
                return 'error', 'EMAIL_DUPLICATED'

        return 'success', {'telegram': telegram, 'email': email}

    def _validate(self, k, pwd):
        if self.request.user.is_authenticated:
            return 'success', 'OK'

        key_obj = None
        if k:
            key_obj = Key.objects.filter(value=k).first()

            if not key_obj:
                return 'error', 'WRONG_KEY'

            usr = key_obj.owner
            if not usr:
                if not pwd:
                    return 'error', 'USER_NOT_ASSIGNED'

                if key_obj.get_level() != 1:
                    return 'error', 'WRONG_KEY'

                res, msg = self._validate_settings()
                if res == 'error':
                    return res, msg

                usr = User.objects.create_user(
                    self.request.POST.get('email'),
                    password=pwd,
                    do_not_create_key=True,
                    level=key_obj.get_level(),
                    email=msg['email'],
                    telegram=msg['telegram']
                )
                usr.save()

                key_obj.owner = usr
                key_obj.is_paid = True
                key_obj.save()

            if not pwd:
                return 'success', 'PASSWORD_REQUIRED'

        if key_obj and pwd:
            user = authenticate(username=usr.username, password=pwd)

            if not user:
                return 'error', 'WRONG_PASSWORD'

            login(self.request, user)

            return 'success', 'OK'

        return 'error', 'LOGIN_ERROR'

    def get_context_data(self, **kwargs):
        k = (self.request.POST.get('key') or '').strip()
        pwd = (self.request.POST.get('password') or '').strip()

        res, msg = self._validate(k, pwd)

        return {
            'result': res,
            'message': msg
        }