# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm, Form
from profiles.models import User


class ContactsForm(ModelForm):
    email = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        ),
        required=True
    )
    telegram = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        ),
        required=True
    )
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'class': 'form-control'}
        ),
        required=False
    )
    class Meta:
        model = User
        fields = ['email', 'telegram', 'phone', 'show_phone_to_partners']


class ChangePasswordForm(Form):
    def __init__(self, user, *args, **kwargs):
        self.user = user
        super().__init__(*args, **kwargs)

    old_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}
        ),
        required=True
    )
    new_password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}
        ),
        required=True
    )
    password_confirm = forms.CharField(
        widget=forms.PasswordInput(
            attrs={'class': 'form-control'}
        ),
        required=True
    )

    def clean_old_password(self):
        data = self.cleaned_data['old_password'].strip()
        if not self.user.check_password(data):
            raise ValidationError('Неправильный старый пароль')
        return data

    def clean_password_confirm(self):
        password_confirm = self.cleaned_data['password_confirm'].strip()
        new_password = self.cleaned_data['new_password'].strip()

        if new_password != password_confirm:
            raise ValidationError('Новый пароль и подтверждение не совпадают')
        return password_confirm
