# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.conf import settings

from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils.translation import ugettext_lazy as _
from keys.models import Key

from .managers import UserManager


class User(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        'Username',
        max_length=255,
        unique=True
    )
    real_name = models.CharField(
        'Real name',
        max_length=500,
        null=True, blank=True
    )
    date_joined = models.DateTimeField(
        'Date joined',
        auto_now_add=True
    )
    is_active = models.BooleanField(
        'Active',
        default=True
    )
    avatar = models.ImageField(
        upload_to='avatars/',
        null=True, blank=True
    )
    email = models.CharField(
        'Email',
        max_length=255,
        null=True, blank=True
    )
    telegram = models.CharField(
        'Telegram',
        max_length=300,
        null=True, blank=True
    )
    phone = models.CharField(
        'Phone',
        max_length=255,
        null=True, blank=True
    )
    show_phone_to_partners = models.BooleanField(
        default=False
    )
    level = models.PositiveIntegerField(
        default=1
    )
    is_staff = models.BooleanField(
        default=False
    )

    def get_current_key(self):
        return self.key_owner.filter(is_secondary=False).order_by('-childs_limit').first()

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def set_key(self):
        if Key.objects.filter(owner=self).exists():
            raise Exception('User already has a key')
        val = Key.gen()
        k = Key.objects.create(
            value=val,
            owner_id=self.pk,
        )
        return k

    def get_income(self):
        cur_key = self.get_current_key()
        cur_tree_level = cur_key.level
        lvls = settings.LEVELS_INFO
        cost_by_level = {v['id']: v['cost'] for k, v in lvls.items()}
        dt = Key.objects.filter(is_paid=True, seller=self).values('level')
        total_cost = 0
        for d in dt:
            idx = d['level'] - cur_tree_level
            try:
                total_cost += cost_by_level[idx]
            except KeyError:
                raise
                pass
        return total_cost

    def get_possible_income(self):
        cur_key = self.get_current_key()
        cur_tree_level = cur_key.level
        ct = Key.objects.filter(seller=self, level=cur_tree_level+1).count()
        return ct * 327680

    def get_partners_count(self):

        return Key.objects.filter(seller=self, is_paid=True).count()

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between.
        '''
        full_name = self.real_name or self.username
        return full_name.strip()

    def get_seller_for_level_up(self):
        if self.level > 3:
            return None
        key = self.get_current_key()
        qs = key.get_ancestors(ascending=True)
        qs = qs[0:5]
        parents = list(qs)
        if not parents:
            return None
        try:
            return parents[self.level].owner
        except IndexError:
            pass

    def get_short_name(self):
        '''
        Returns the short name for the user.
        '''
        return self.username

    def email_user(self, subject, message, from_email=None, **kwargs):
        '''
        Sends an email to this User.
        '''
        return
        send_mail(subject, message, from_email, [self.email], **kwargs)
