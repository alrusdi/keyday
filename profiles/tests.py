# -*- coding: utf-8 -*-
from django.test import TestCase
from keys.models import Key
from profiles.models import User


class UserTest(TestCase):

    def test_correctly_counts_income(self):
        main_user = User.objects.create(
            username='user1',
            level=4
        )

        main_user_key = Key.objects.create(
            owner=main_user,
            value=Key.gen()
        )

        another_user = User.objects.create(
            username='user2',
            level=4
        )

        another_user_key = Key.objects.create(
            owner=another_user,
            value=Key.gen()
        )


        # Create lvl 1 keys => 10 euro
        kl1_1 = Key.objects.create(
            seller=main_user,
            parent=main_user_key,
            is_paid=True,
            value=Key.gen()

        )
        kl1_2 = Key.objects.create(
            seller=main_user,
            parent=main_user_key,
            is_paid=False,
            value=Key.gen()

        )
        kl1_3 = Key.objects.create(
            seller=another_user,
            parent=main_user_key,
            is_paid=True,
            value=Key.gen()
        )

        # Create lvl 2 keys => 20+20=40
        kl2_1 = Key.objects.create(
            seller=main_user,
            parent=kl1_1,
            is_paid=True,
            value=Key.gen()

        )
        kl2_2 = Key.objects.create(
            seller=main_user,
            parent=kl1_1,
            is_paid=True,
            value=Key.gen()

        )
        kl2_3 = Key.objects.create(
            seller=another_user,
            parent=kl1_3,
            is_paid=True,
            value=Key.gen()
        )

        # Create lvl 3 keys => 160+160=320 euro
        kl3_1 = Key.objects.create(
            seller=main_user,
            parent=kl2_1,
            is_paid=True

        )
        kl3_2 = Key.objects.create(
            seller=main_user,
            parent=kl2_2,
            is_paid=True,
            value=Key.gen()

        )
        kl3_3 = Key.objects.create(
            seller=another_user,
            parent=kl2_3,
            is_paid=True,
            value=Key.gen()
        )

        # Create lvl 4 keys => 5120+5120=10240
        kl4_1 = Key.objects.create(
            seller=main_user,
            parent=kl3_1,
            is_paid=True,
            value=Key.gen()

        )
        kl4_2 = Key.objects.create(
            seller=main_user,
            parent=kl3_1,
            is_paid=True,
            value=Key.gen()

        )
        kl4_3 = Key.objects.create(
            seller=another_user,
            parent=kl3_3,
            is_paid=True,
            value=Key.gen()
        )

        self.assertEqual(
            main_user.get_income(),
            10+40+320+10240
        )


    def test_correctly_counts_possible_income(self):
        main_user = User.objects.create(
            username='user1',
            level=4
        )

        main_user_key = Key.objects.create(
            owner=main_user,
            value=Key.gen()
        )

        # Create lvl 1 keys
        kl1_1 = Key.objects.create(
            seller=main_user,
            parent=main_user_key,
            is_paid=True,
            value=Key.gen()

        )
        kl1_2 = Key.objects.create(
            seller=main_user,
            parent=main_user_key,
            is_paid=False,
            value=Key.gen()

        )

        # Create lvl 2 keys => this shouldn't be count
        kl2_1 = Key.objects.create(
            seller=main_user,
            parent=kl1_1,
            is_paid=True,
            value=Key.gen()

        )

        self.assertEqual(
            main_user.get_possible_income(),
            327680 + 327680
        )
