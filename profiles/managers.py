# -*- coding: utf-8 -*-
from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, do_not_create_key=False, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not username:
            raise ValueError('Username must be set')
        user = self.model(username=username, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)

        if not do_not_create_key:
            user.set_key()

        return user

    def create_user(self, username, password=None, do_not_create_key=False, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, do_not_create_key=do_not_create_key, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)
