"use strict";

var server_side_call_in_progress = false;
var form_id = 'signup';
var login_form;

var set_state = function (to) {
    login_form.attr('class', to)
};

var lock_ss = function () {
    server_side_call_in_progress = true;
    setTimeout(function (){
        server_side_call_in_progress = false;
    }, 2000)
};

var form_error = function (err) {
    $('.error-text').text(trans(err)).fadeIn();
};

var check_key = function () {
    lock_ss();
    $.post(
        '/login/',
        {
            'key': login_form.find('#control-name').val(),
            'csrfmiddlewaretoken': login_form.find('[name=csrfmiddlewaretoken]').val(),
            'password': login_form.find('#control-password').val(),
            'telegram': login_form.find('#control-phone').val(),
            'email': login_form.find('#control-email').val()
        },
        function (data) {
            server_side_call_in_progress = false;
            if (data.message == 'OK') {
                window.location.href = '/keys/';
                return;
            }

            if (data.result == 'success' && data.message == 'PASSWORD_REQUIRED') {
                form_id = "password";
                change_form_state();
            }

            if (data.result == 'error') {
                if (data.message == 'WRONG_KEY') {
                    form_error('KEY_IS_NOT_FOUND');
                } else if (data.message == 'WRONG_PASSWORD') {
                    form_error('WRONG_PASSWORD');
                } else if (data.message == 'USER_NOT_ASSIGNED') {
                    login_form.find('#control-password').attr('placeholder', trans('CREATE_YOUR_PASSWORD'));
                    form_id='password';
                    change_form_state();
                } else if (data.message == 'TELEGRAM_REQUIRED') {
                    form_id = 'phone';
                    change_form_state();
                } else if (data.message == 'EMAIL_REQUIRED') {
                    form_id = 'email';
                    change_form_state();
                } else if (data.message == 'EMAIL_DUPLICATED') {
                    form_id = 'email';
                    form_error('EMAIL_DUPLICATED');
                    change_form_state();
                } else if (data.message == 'PASSWORD_CONFIRMATION_REQUIRED') {
                    form_id = 'password-repeat';
                    change_form_state();
                }
            }
        }
    );
};

var change_form_state = function () {
    if (server_side_call_in_progress) return;
    var prev_state = login_form.attr('class');
    switch (form_id) {
        case 'name':
            if ( ! $('#control-name').val()) {
                form_error('EMPTY_KEY');
                break;
            }
            check_key();
            break;
        case "phone":
            if (prev_state == "phone") {
                if (!$('#control-password').val()) {
                    form_error('EMPTY_TELEGRAM');
                } else {
                    check_key();
                }
            }
            set_state('phone');
            break;
        case "email":
            if (prev_state == "email") {
                if ( ! $('#control-email').val()) {
                    form_error('EMPTY_EMAIL');
                } else {
                    check_key();
                }
            }
            set_state('email');
            break;
        case "password":
            if (prev_state == "password") {
                if (! $('#control-password').val()) {
                    form_error('EMPTY_PASSWORD');
                } else {
                    check_key();
                }
                break;
            }
            set_state("password");
            break;
        case "password-repeat":
            if (prev_state == "password-repeat") {
                var pwd = login_form.find('#control-password').val();
                var pwd_rep = login_form.find('#control-password-repeat').val();
                if ( ! pwd_rep) {
                    form_error('EMPTY_PASSWORD_CONFIRM');
                } else if (pwd != pwd_rep)  {
                    form_error('PASSWORDS_NOT_MATCH');
                } else {
                    form_id = 'phone';
                    change_form_state();
                }
                break;
            }
            set_state('password-repeat');
            break;
    }
    login_form.find('.icon-action').addClass('back');
};

var setup_login_form = function () {
    login_form = $('#login_form');

    login_form.find('.form-head').click( function() {
        set_state("name");
        form_id='name';
    });

    login_form.find('.form-action').click(function () {
        change_form_state()
    });
};

$(function () {
    setup_login_form();
    $('input').on('keyup', function(){
       $('.grop-from').removeClass('error');
       $('.error-text').fadeOut();

        if($(this).val()!=''){
          $('.icon-action').removeClass('back');
        } else{
          $('.icon-action').addClass('back');
        }
    });
});
