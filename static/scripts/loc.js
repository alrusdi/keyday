var trans;
var trans_data = {};
$(function () {
    var tr_file = $('#loc').attr('src');
    $.get(tr_file, function (data) {
        trans_data = data;
        trans = function (str) {
            var tr = trans_data[str];
            if (tr) {
                return tr;
            }
            return str;
        }
    })
});
