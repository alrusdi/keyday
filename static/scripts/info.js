(function ($) {
    'use strict';


    var hide_error = function () {
        $('#key-error-msg-cont').hide();
        $('#key-error-msg-txt').html('');

    };

    var show_error = function (err) {
        $('#key-error-msg-txt').html(trans(err));
        $('#key-error-msg-cont').show();
    };

    $('#key_activate').on('keypress change', function (e) {
        var key = $(this).val();
        hide_error();
        if ( ! key) return;
        if ((e.type == 'keypress' && e.which == 13) || e.type=='change') {
            $.ajax({
                url: '/commit-key/'+key+'/',
                type: 'GET',
                success: function(data){
                    if (data.result == 'success') {
                        window.location.href = window.location.href;
                        return;
                    }
                    switch (data.message) {
                        case 'WRONG_KEY':
                            show_error('WRONG_KEY');
                            break;
                        case 'TOO_LOW_LEVEL':
                            show_error('TOO_LOW_LEVEL');
                            break;
                        case 'INCORRECT_SELLER':
                            show_error('INCORRECT_SELLER');
                            break;
                        case 'INCORRECT_KEY_LEVEL':
                            show_error('INCORRECT_KEY_LEVEL');
                            break;
                    }
                },
                error: function(data) {
                    show_error('KEY_IS_NOT_FOUND');
                }
            });
        }
    })
})(jQuery);