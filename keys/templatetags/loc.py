# -*- coding: utf-8 -*-
from django import template
from common.helpers import translate
from django.utils.text import capfirst

register = template.Library()


@register.simple_tag(takes_context=True)
def trans(context, needed_string, uppercase_first=False):
    lang = context.get('current_lang')
    if lang:
        ret = translate(needed_string, lang=lang)
    else:
        ret = needed_string

    if uppercase_first:
        ret = capfirst(ret)
    return ret


@register.tag
def monkey_patch_pagination(parser, token):
    nodelist = parser.parse(('end_monkey_patch_pagination',))
    parser.delete_first_token()
    return UpperNode(nodelist)


class UpperNode(template.Node):
    def __init__(self, nodelist):
        self.nodelist = nodelist

    def render(self, context):
        pag = self.nodelist.render(context)
        if '<a ' in pag:
            pag = pag.replace('<a ', '<li class="footable-page-arrow"><a ')
            pag = pag.replace('</a>', '</a></li>')

            pag = pag.replace('<span ', '<li class="footable-page-arrow active"><a ')
            pag = pag.replace('</span>', '</a></li>')

        return pag