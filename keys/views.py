from common.views import JsonView
from django.conf import settings
from django.contrib.auth.decorators import login_required

# Create your views here.
from django.http import Http404
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic import TemplateView
from keys.models import Key

decorators = [never_cache, login_required]


@method_decorator(decorators, name='dispatch')
class CreateKeyView(JsonView):

    def _validate(self, usr, lvl):
        if lvl > usr.level:
            return 'error', 'WRONG_LEVEL'

        level_info = settings.LEVELS_INFO[lvl]
        lim = level_info['limit']
        cur_count = Key.objects.filter(seller=usr, childs_limit=lim).count()

        if lim <= cur_count and usr.level == level_info['id']:
            return 'error', 'LEVEL_COMPLETE'

        parent_key = usr.get_current_key()
        if not parent_key:
            return 'error', 'PARENT_KEY_DOES_NOT_EXIST'

        value = Key.gen()
        k = Key.objects.create(
            seller=usr,
            childs_limit=lim,
            value=value,
            parent=parent_key,
            initial_limit=lim
        )

        return 'success', str(k.value)

    def get_context_data(self, **kwargs):
        usr = self.request.user
        lvl = self.kwargs['level']

        res, msg = self._validate(usr, lvl)

        return {
            'result': res,
            'message': msg
        }


@method_decorator(decorators, name='dispatch')
class CommitKeyView(JsonView):

    def _validate(self, usr, key_val):
        new_key = Key.objects.filter(owner_id__isnull=True, value=key_val).first()
        if not new_key:
            return 'error', 'WRONG_KEY'

        cur_key = usr.get_current_key()

        if cur_key.childs_limit >= new_key.childs_limit:
            return 'error', 'TOO_LOW_LEVEL'

        seller = usr.get_seller_for_level_up()

        if seller.pk != new_key.seller.pk:
            return 'error', 'INCORRECT_SELLER'

        wanted_level = usr.level + 1

        if wanted_level != new_key.get_level():
            return 'error', 'INCORRECT_KEY_LEVEL'

        usr.level = wanted_level
        usr.save()

        cur_key.childs_limit = new_key.childs_limit
        cur_key.save()

        new_key.is_secondary = True
        new_key.is_paid = True
        new_key.owner = usr
        new_key.save()

        return 'success', 'OK'

    def get_context_data(self, **kwargs):
        usr = self.request.user
        key_val = self.kwargs['key']

        res, msg = self._validate(usr, str(key_val))

        return {
            'result': res,
            'message': msg
        }


@method_decorator(decorators, name='dispatch')
class ViewKeyView(TemplateView):
    template_name = 'view_key.html'

    def _validate(self, key_val):
        new_key = Key.objects.filter(seller=self.request.user, value=key_val).first()
        if new_key:
            return new_key.value

    def get_context_data(self, **kwargs):
        k = self._validate(str(self.kwargs['key']))
        if not k:
            raise Http404
        return {
            'key': k
        }
