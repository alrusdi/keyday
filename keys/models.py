# -*- coding: utf-8 -*-
import uuid
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Key(MPTTModel):
    title = models.CharField(
        max_length=200,
        null=True, blank=True
    )
    value = models.CharField(
        max_length=255, unique=True
    )
    parent = TreeForeignKey(
        'self',
        null=True, blank=True,
        related_name='children',
        db_index=True,
        on_delete=models.PROTECT
    )
    is_paid = models.BooleanField(
        default=False
    )
    owner = models.ForeignKey(
        'profiles.User',
        related_name='key_owner',
        null=True, blank=True,
        on_delete=models.SET_NULL
    )
    seller = models.ForeignKey(
        'profiles.User',
        related_name='key_seller',
        null=True, blank=True,
        on_delete=models.SET_NULL
    )
    childs_limit = models.PositiveIntegerField(
        default=4
    )
    initial_limit = models.PositiveIntegerField(
        default=4
    )
    date_created = models.DateTimeField(
        # 'Created at',
        auto_now_add=True
    )
    is_secondary = models.BooleanField(
        default=False
    )

    @property
    def status(self):
        if self.owner:
            return 'STATUS_ALREADY_USED'
        return 'STATUS_ACTIVE'

    def __str__(self):
        own = self.owner
        ret = self.value
        if own:
            ret = '%s (%s)' % (ret, own)
        return ret

    @property
    def status_css(self):
        return 'success' if self.owner else 'warn'

    def get_level(self):
        if self.childs_limit == 4:
            return 1
        if self.childs_limit == 16:
            return 2
        if self.childs_limit == 64:
            return 3
        if self.childs_limit > 64:
            return 4

    @classmethod
    def gen(cls):
        return str(uuid.uuid4().int)
