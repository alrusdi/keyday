from django.contrib import admin
from django_mptt_admin.admin import DjangoMpttAdmin
from .models import Key


@admin.register(Key)
class KeyAdmin(DjangoMpttAdmin):
    pass
